#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from quickfix import Application
from connector.log import setup_logger
from abc import ABC, abstractmethod
import logging


setup_logger("app_log", "logs/message.log")


class BaseApplication(Application, ABC):
    """
    A FIX Application abstract class to be used as Base for
    any new FIX Application.

    ...

    Attributes
    ----------
        response_q: Queue()
            queue to send all the responses from the market to the api.
        settings: dict
            settings of the fix market.
        log: Logger()
            logger of the fix application.
    """

    def __init__(self, response_q, settings):
        """
        Constructs all the necessary attributes for the FIX Application.

        Parameters
        ----------
            response_q: Queue()
                queue to send all the responses from the market to the api.
            settings: dict
                settings of the fix market.
        """

        self.response_q = response_q
        self.settings = settings
        self.log = logging.getLogger("app_log")
        super(BaseApplication, self).__init__()

    @abstractmethod
    def toAdmin(self, message, session):
        """Notification of admin message being sent to target."""
        pass

    @abstractmethod
    def fromAdmin(self, message, session):
        """Notification of admin message being received from target."""
        pass

    @abstractmethod
    def toApp(self, message, session):
        """Notification of app message being sent to target."""
        pass

    @abstractmethod
    def fromApp(self, message, session):
        """Notification of app message being received from target."""
        pass

    def onCreate(self, session):
        """Notification of a session begin created."""
        self.session = session
        print("Starting FIX Application")
        self.log.debug(f"onCreate session ID: {session}")

    def onLogon(self, session):
        """Notification of a session successfully logging on."""
        self.log.debug(f"onLogon session ID: {session}")

    def onLogout(self, session):
        """Notification of a session logging off or disconnecting."""
        self.log.debug(f"onLogout session ID: {session}")


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================