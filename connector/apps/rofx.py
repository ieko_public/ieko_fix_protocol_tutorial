#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from connector.apps.utils import get_header_value, clear_message
from connector.apps.base import BaseApplication
import quickfix as fix


class ROFXApplication(BaseApplication):
    """
    A FIX Application class to connect to ROFX.
    ...

    Attributes
    ----------
        response_q: Queue()
            queue to send all the responses from the market to the api.
        settings: dict
            settings of the fix market.
        log: Logger()
            logger of the fix application.
    """

    def toAdmin(self, message, session):
        """Notification of admin message being sent to target."""
        self.log.debug(f"send toAdmin {clear_message(message)}. Session {session}")
        if get_header_value(message, fix.MsgType()) == fix.MsgType_Logon:
            message.getHeader().setField(553, self.settings.Username)
            message.getHeader().setField(554, self.settings.Password)
            self.log.debug(f"send Logon {clear_message(message)}. Session {session}")

    def fromAdmin(self, message, session):
        """Notification of admin message being received from target."""
        self.log.debug(f"receive fromAdmin {clear_message(message)}. Session {session}")

    def toApp(self, message, session):
        """Notification of app message being sent to target."""
        self.log.debug(f"send toApp {clear_message(message)}")

    def fromApp(self, message, session):
        """Notification of app message being received from target."""
        msg_type = get_header_value(message, fix.MsgType())
        self.log.debug(f"receive fromApp {clear_message(message)}, type {msg_type}")
        if not msg_type.lower() == "h":
            self.response_q.put({"type": msg_type, "msg": clear_message(message)})

    def send(self, message):
        """Send the message to the ROFX Market"""
        fix.Session.sendToTarget(message, self.session)


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================