#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from munch import Munch
import yaml

"""Reads the ieko_public_fix_connector.yml to get all the config and converts it to a Munch object."""
with open("config/ieko_public_fix_connector.yml", "r") as f:
    config = Munch.fromDict(yaml.load(f, Loader=yaml.FullLoader))


def get_recommendations():
    """
    Returns a Munch dict with the recommendations.

    Those recommendations are stored in ieko_public_fix_connector.yml.
    """
    return config.Recommendations


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================