#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from quickfix import SessionSettings, FileStoreFactory, FileLogFactory, SocketInitiator
from connector.messages.rofx import message_creator
from connector.clients.base import FIXClient
from string import Template
import configparser
import time


class ROFXClient(FIXClient):
    """
    An FIX Client for ROFX market.
    ...

    Attributes
    ---------
        fix_app: ROFXApplication()
            the fix application this client will use to connect to ROFX.
        config: Munch()
            a Munch dict with the config data for this client.
        client: SocketInitiator
            instance of the fix initiatior used as client.
    """

    def create(self):
        """Creates the client instance as the attribute self.client."""
        settings = SessionSettings(self.config.ConnectorCfg)
        storefactory = FileStoreFactory(settings)
        logfactory = FileLogFactory(settings)
        self.client = SocketInitiator(self.fix_app, storefactory, settings, logfactory)

    def start(self):
        """
        Start the connection to ROFX.

        If the attribute self.client is not setted (the instance is not created)
        it will call the method create() to do its work.
        """
        self.set_config()
        if not self.client:
            self.create()
        print("Starting ROFX Client...")
        self.client.start()

    def stop(self):
        """stops the connection to ROFX"""
        self.client.stop()

    def set_config(self):
        """
        Sets the required fields in the config file.

        Opens the ConnectorCfg file and sets the SenderCompID value.
        """
        conf = configparser.ConfigParser()
        conf.readfp(open(self.config.ConnectorCfg))
        conf.set("SESSION", "SenderCompID", self.config.Username)
        conf.set("SESSION", "SocketConnectPort", str(self.config.Port))
        conf.set("SESSION", "SocketConnectHost", str(self.config.Host))
        conf.set("SESSION", "TargetCompID", "ROFX")
        with open(self.config.ConnectorCfg, "w") as cfile:
            conf.write(cfile)

    def wait_until_is_logged(self, count=10, seconds=1):
        """Blocks until client is logged on."""
        counter = 1

        while not self.client.isLoggedOn():
            if counter == count:
                print(f"Couldnt login after {count * seconds} seconds...")
            counter += 1
            time.sleep(seconds)

        return self.client.isLoggedOn()

    def wait_until_logout(self, count=10, seconds=1):
        """Blocks until client is logged on."""
        counter = 1

        while self.client.isLoggedOn():
            if counter == count:
                raise Exception(f"Couldnt login after {count * seconds} seconds")
            counter += 1
            time.sleep(seconds)

        return not self.client.isLoggedOn()

    # ----Messages-To-Market----

    def get_market_data(self, request: dict):
        """Parse and sends the Market Data Request to ROFX."""
        new_id = request["id"]
        instrument = request["instrument"]

        return message_creator.market_data_request(self.fix_app, new_id, instrument)

    def create_new_order(self, request: dict):
        """Parse and sends the New Order Request to ROFX."""
        clordid = request["clordid"]
        instrument = request["instrument"]
        ordtype = request["ordtype"]
        orderqty = request["orderqty"]
        price = request["price"]
        side = request["side"]
        account = request["account"]

        return message_creator.new_order_single_request(
            self.fix_app, clordid, instrument, ordtype, orderqty, price, side, account
        )

    def cancel_order(self, request: dict):
        """Parse and sends the Cancel Order Request to ROFX."""
        clordid = request["clordid"]
        origclorid = request["origclorid"]
        instrument = request["instrument"]
        side = request["side"]
        account = request["account"]

        return message_creator.cancel_order_request(
            self.fix_app, clordid, origclorid, instrument, side, account
        )

    def logout(self, request: dict):
        """Sends the Logout Request to ROFX."""
        return message_creator.logout(self.fix_app)


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================