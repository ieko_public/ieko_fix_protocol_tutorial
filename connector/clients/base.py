#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from abc import ABC, abstractmethod


class FIXClient(ABC):
    """
    An abstract class to be used as Base for any FIX Client class.
    ...

    Attributes
    ----------
        fix_app: BaseApplication() Subclass object
            the fix application this client will use.
        config: dict
            dict with the config data for this client.
        client: SocketInitiator
            instance of the fix initiatior used as client.
    """

    def __init__(self, fix_app, config):
        """
        Constructs all the necessary attributes for the FIX Client.

        Parameters
        ----------
            fix_app: BaseApplication() Subclass object
                the fix application this client will use.
            config: dict
                dict with the config data for this client.
        """
        self.fix_app = fix_app
        self.config = config
        self.client = None

    @abstractmethod
    def create(self):
        """creates the client instance."""
        pass

    @abstractmethod
    def start(self):
        """start the connection to the fix market."""
        pass

    @abstractmethod
    def stop(self):
        """stops the connection to the fix market."""
        pass


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================