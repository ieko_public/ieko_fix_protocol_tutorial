#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from connector.clients.rofx import ROFXClient
from connector.apps.rofx import ROFXApplication
from connector.settings import config
from connector.messages.rofx import message_creator
from threading import Thread
from queue import Queue
from time import sleep


class Connector:
    """
    class that handles the connection to any FIX Market.
    """

    def __init__(self, resp_q: Queue, market: str):
        """Constructs all the nessesary attributes."""
        self.response_q = resp_q
        self.request_q = Queue()
        self.market = market
        self._set_client()

    def start(self):
        """Creates the connector thread and starts it."""
        self.connector = Thread(target=self._run_client, args=(), daemon=True)
        self.connector.start()
        return self.request_q

    def stop(self):
        """Stops the connector thread."""
        self.client.wait_until_logout()
        self.client.stop()
        self.connector.join()

    # -----Private-Methods-----

    def _run_client(self):
        """Sets the client, starts it and starts the request listener."""
        self.client.start()
        self.client.wait_until_is_logged()
        self._start_request_listener()

    def _set_client(self):
        """Sets the config, the FIX app and FIX client."""
        print("Setting FIX Client")
        if self.market.lower() == "rofx":
            fix_config = config.ROFX.Fix
            fix_app = ROFXApplication(self.response_q, fix_config)
            self.client = ROFXClient(fix_app, fix_config)

    def _start_request_listener(self):
        """Starts listening in the request_q and identifies the request types."""
        while True:
            request = self.request_q.get()
            if request:
                if request["type"].lower() == "market_data":
                    self.client.get_market_data(request)
                elif request["type"].lower() == "new_order":
                    self.client.create_new_order(request)
                elif request["type"].lower() == "cancel_order":
                    self.client.cancel_order(request)
                elif request["type"].lower() == "quit":
                    self.client.logout(request)
                    break


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================