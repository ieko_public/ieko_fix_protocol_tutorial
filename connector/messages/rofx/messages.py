#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from connector.messages.rofx.utils import type_is_snapshot_plus_update
from connector.messages.rofx import fields
from connector.messages.base import Message
import quickfix as fix


class MarketDataRequest(Message):
    """An model class for the market data request (MsgType V)."""

    AggregatedBook = fields.AggregatedBook()
    MarketDepth = fields.MarketDepth()
    MDReqID = fields.MDReqID()
    MDUpdateType = fields.MDUpdateType()
    SubscriptionRequestType = fields.SubscriptionRequestType()

    _instruments = []

    @property
    def instruments(self):
        """Returns the instruments to get the market data from"""
        return self._instruments

    @instruments.setter
    def instruments(self, instrument):
        """Adds an instrument to get the market data from"""
        if isinstance(instrument, list):
            self._instruments = instrument
            return
        self._instruments.append(instrument)

    @property
    def required_fields(self):
        """Returns the requires fields in the correct order as a list """
        fields = [
            self.instruments,
            self.MDReqID,
            self.SubscriptionRequestType,
            self.MarketDepth,
            self.AggregatedBook,
        ]

        if self.SubscriptionRequestType and type_is_snapshot_plus_update(
            self.SubscriptionRequestType
        ):
            fields.append(self.MDUpdateType)
        return fields

    def is_valid(self):
        """Analyzes the fields, if some required field is empty returns false"""
        missing_values = [field for field in self.required_fields if field is None]
        return not bool(missing_values)

    @property
    def message(self):
        """Creates the Fix Message."""

        # Assert message is valid
        assert self.is_valid(), "Validate required fields."

        # ---Create-Message---
        message = Message.new_message(msg_type=fix.MsgType_MarketDataRequest)

        # ---Body---
        message.setField(self.MDReqID)
        message.setField(self.SubscriptionRequestType)
        message.setField(self.MarketDepth)
        if type_is_snapshot_plus_update(self.SubscriptionRequestType):
            message.setField(self.MDUpdateType)
        message.setField(self.AggregatedBook)

        # --- MDEntryTypes ---
        import quickfix50 as fix50

        group = fix50.MarketDataRequest().NoMDEntryTypes()

        group.setField(fix.MDEntryType(fix.MDEntryType_BID))
        message.addGroup(group)
        group.setField(fix.MDEntryType(fix.MDEntryType_OFFER))
        message.addGroup(group)
        group.setField(fix.MDEntryType(fix.MDEntryType_TRADE))
        message.addGroup(group)
        group.setField(fix.MDEntryType(fix.MDEntryType_OPENING_PRICE))
        message.addGroup(group)
        group.setField(fix.MDEntryType(fix.MDEntryType_CLOSING_PRICE))
        message.addGroup(group)

        # ---Instruments---
        symbol = fix50.MarketDataRequest().NoRelatedSym()
        for instrument in self.instruments:
            symbol.setField(fix.Symbol(instrument))
            message.addGroup(symbol)

        return message


class NewOrderSingle(Message):
    """An model class for the New Order Sngle Request (MsgType=D)."""

    Side = fields.Side()
    Price = fields.Price()
    Symbol = fields.Symbol()
    OrdType = fields.OrdType()
    Account = fields.Account()
    ClOrdID = fields.ClOrdID()
    OrderQty = fields.OrderQty()

    @property
    def required_fields(self):
        """Returns the requires fields in the correct order as a list """

        fields = [
            self.Account,
            self.ClOrdID,
            self.OrderQty,
            self.OrdType,
            self.Side,
            self.Symbol,
        ]

        # Price is required for ord types LIMIT and STOP LIMIT
        if self.OrdType in [fix.OrdType_LIMIT, fix.OrdType_STOP_LIMIT]:
            fields.append(self.Price)

        return fields

    def is_valid(self):
        """Analyzes the fields, if some required field is empty returns false"""
        missing_values = [field for field in self.required_fields if field is None]
        return not bool(missing_values)

    @property
    def message(self):
        """Creates the FIX Message."""

        # Assert message is valid
        assert self.is_valid(), "Validate required fields."

        # ---Create-Message---
        message = Message.new_message(
            msg_type=fix.MsgType_NewOrderSingle, set_transact_time=True
        )

        # ---Body---
        message.setField(self.Account)
        message.setField(self.ClOrdID)
        message.setField(self.Symbol)
        message.setField(self.OrderQty)
        message.setField(self.OrdType)
        message.setField(self.Price)
        message.setField(self.Side)

        return message


class OrderCancelRequest(Message):
    """An model class for the New Order Sngle Request (MsgType=D)."""

    Side = fields.Side()
    Symbol = fields.Symbol()
    Account = fields.Account()
    OrderId = fields.OrderId()
    ClOrdID = fields.ClOrdID()
    OrderQty = fields.OrderQty()
    OrigClOrdID = fields.OrigClOrdID()

    @property
    def required_fields(self):
        """Returns the requires fields in the correct order as a list """

        fields = [self.Account, self.ClOrdID, self.OrderQty, self.Side, self.Symbol]

        if not self.OrderId:  # if not OrderID OrigClOrdID is required
            fields.append(self.OrigClOrdID)

        return fields

    def is_valid(self):
        """Analyzes the fields, if some required field is empty returns false"""
        missing_values = [field for field in self.required_fields if field is None]
        return not bool(missing_values)

    @property
    def message(self):
        """Creates the FIX Message."""

        # Assert message is valid
        assert self.is_valid(), "Validate required fields."

        # ---Create-Message---
        message = Message.new_message(
            msg_type=fix.MsgType_OrderCancelRequest, set_transact_time=True
        )

        # ---Body---
        message.setField(self.Account)
        message.setField(self.ClOrdID)

        if self.OrderId:
            message.setField(self.OrderId)
        else:
            message.setField(self.OrigClOrdID)

        message.setField(self.Side)
        message.setField(self.Symbol)
        message.setField(self.OrderQty)

        return message


class Logout(Message):
    """An model class for the New Order Sngle Request (MsgType=D)."""

    @property
    def required_fields(self):
        """No required fields."""
        pass

    def is_valid(self):
        """The message will always be valid."""
        return True

    @property
    def message(self):
        """Creates the FIX Message."""

        # Assert message is valid
        assert self.is_valid(), "Validate required fields."

        # ---Create-Message---
        message = Message.new_message(msg_type=fix.MsgType_Logout)

        return message


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================