#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from connector.messages.rofx.utils import get_order_type
from connector.messages.rofx import messages
from connector.apps.rofx import ROFXApplication
import quickfix as fix


def market_data_request(app: ROFXApplication, new_id: str, instrument: str):
    """
    Creates the market data request using the Messages and Fields descriptors.
    """

    instruments = []
    instruments.append(instrument)

    # ---Creates-Order---
    order = messages.MarketDataRequest()
    order.SubscriptionRequestType = fix.SubscriptionRequestType_SNAPSHOT_PLUS_UPDATES
    order.MDReqID = str(new_id)
    order.MarketDepth = 5
    order.instruments = instrument
    order.MDUpdateType = fix.MDUpdateType_FULL_REFRESH
    order.AggregatedBook = True

    # ---Validate-Order-And-Send-To-Market---
    if order.is_valid():
        print("Sending Market Data Request...")
        app.send(order.message)
        return True

    return False


def new_order_single_request(
    app: ROFXApplication,
    clordid: str,
    instrument: str,
    ordtype: str,
    orderqty: int,
    price: float,
    side: str,
    account: str,
):
    """
    Creates the new order single request using the Messages and Fields descriptors.
    """

    # ---Creates-Order---
    order = messages.NewOrderSingle()
    order.ClOrdID = clordid
    order.Account = account
    order.Symbol = instrument
    order.OrdType = get_order_type(ordtype)
    order.OrderQty = orderqty
    order.Price = price

    # Condition to set the correct value to Side
    if str(side).upper() == "BUY":
        order.Side = fix.Side_BUY
    elif str(side).upper() == "SELL":
        order.Side = fix.Side_SELL
    else:
        print("Side not valid.")
        return None

    # ---Validate-Order-And-Send-To-Market---
    if order.is_valid():
        app.send(order.message)
        return order.ClOrdID

    return None  # None indicate False on place a order ==> none ClOrdID


def cancel_order_request(
    app: ROFXApplication,
    clordid: str,
    origclorid: str,
    instrument: str,
    side: str,
    account: str,
    orderqty: int = 1,
):
    """
    Creates the cancel order request using the Messages and Fields descriptors.
    """

    # ---Creates-Order---
    order = messages.OrderCancelRequest()
    order.ClOrdID = clordid
    order.OrigClOrdID = origclorid
    order.Account = account
    order.Symbol = instrument
    order.OrderQty = orderqty
    # OrderQty field is needed for protocol but not used by market

    # Condition to set the correct value to Side
    if str(side).upper() == "BUY":
        order.Side = fix.Side_BUY
    elif str(side).upper() == "SELL":
        order.Side = fix.Side_SELL
    else:
        print("Side not valid.")
        return None

    # ---Validate-Order-And-Send-To-Market---
    if order.is_valid():
        app.send(order.message)
        return order.ClOrdID

    return None  # None indicate False on place a order ==> none ClOrdID


def logout(app: ROFXApplication):
    """Creates the Logout request."""
    order = messages.Logout()

    if order.is_valid():
        app.send(order.message)
        return True

    return False


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================