#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from connector.messages.base import Field
import quickfix as fix
import uuid


class Account(Field):
    """Descriptor of the FIX field Account."""

    def __set__(self, obj, value):
        obj.__dict__[self.name] = fix.Account(str(value))


class AggregatedBook(Field):
    """Descriptor of the FIX field AggregatedBook."""

    YES = True
    NO = False

    valid_values = [
        YES,
        NO,
    ]

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        if value not in self.valid_values:
            raise ValueError(f"Invalid value, the valid values are {self.valid_values}")
        obj.__dict__[self.name] = fix.AggregatedBook(value)


class ClOrdID(Field):
    """Descriptor of the FIX field ClOrdId."""

    def __get__(self, obj, type=None):
        """If not ClOrdID, generate one."""
        if not obj.__dict__.get(self.name):
            clordid = fix.ClOrdID(uuid.uuid1().hex)
            obj.__dict__[self.name] = clordid
        return obj.__dict__.get(self.name)

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        obj.__dict__[self.name] = fix.ClOrdID(str(value))


class MarketDepth(Field):
    """Descriptor of the FIX field MarketDepth."""

    FULL_BOOK = 0
    TOP_OF_BOOK = 1
    DEPTH_TWO = 2
    DEPTH_THREE = 3
    DEPTH_FOUR = 4
    DEPTH_FIVE = 5

    valid_values = [
        FULL_BOOK,
        TOP_OF_BOOK,
        DEPTH_TWO,
        DEPTH_THREE,
        DEPTH_FOUR,
        DEPTH_FIVE,
    ]

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        if value not in self.valid_values:
            raise ValueError(f"Invalid value, the valid values are {self.valid_values}")
        obj.__dict__[self.name] = fix.MarketDepth(value)


class MDEntryType(Field):
    """Descriptor of the FIX field MDEntryType."""

    BID = fix.MDEntryType_BID
    OFFER = fix.MDEntryType_OFFER
    TRADE = fix.MDEntryType_TRADE
    OPENING_PRICE = fix.MDEntryType_OPENING_PRICE
    CLOSING_PRICE = fix.MDEntryType_CLOSING_PRICE
    SETTLEMENT_PRICE = fix.MDEntryType_SETTLEMENT_PRICE
    TRADING_HIGH_PRICE = fix.MDEntryType_TRADING_SESSION_HIGH_PRICE
    TRADING_LOW_PRICE = fix.MDEntryType_TRADING_SESSION_LOW_PRICE
    TRADE_VOLUME = fix.MDEntryType_TRADE_VOLUME
    OPEN_INTEREST = fix.MDEntryType_OPEN_INTEREST

    valid_values = [
        BID,
        OFFER,
        TRADE,
        OPENING_PRICE,
        CLOSING_PRICE,
        SETTLEMENT_PRICE,
        TRADING_HIGH_PRICE,
        TRADING_LOW_PRICE,
        TRADE_VOLUME,
        OPENING_PRICE,
    ]

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        if value not in self.valid_values:
            raise ValueError(f"Invalid value, the valid values are {self.valid_values}")
        obj.__dict__[self.name] = fix.MDEntryType(value)


class MDReqID(Field):
    """Descriptor of the FIX field Market Data Request ID (String 32)."""

    def __get__(self, obj, type=None):
        """If not MDReqID, generate one."""
        if not obj.__dict__.get(self.name):
            mdreqid = fix.MDReqID(uuid.uuid1().hex)
            obj.__dict__[self.name] = mdreqid
        return obj.__dict__.get(self.name)

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        obj.__dict__[self.name] = fix.MDReqID(str(value))


class MDUpdateType(Field):
    """Descriptor of the FIX field MDUpdateType."""

    FULL_REFRESH = fix.MDUpdateType_FULL_REFRESH

    valid_values = [
        FULL_REFRESH,
    ]

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        if value not in self.valid_values:
            raise ValueError(f"Invalid value, the valid values are {self.valid_values}")
        obj.__dict__[self.name] = fix.MDUpdateType(value)


class OrderId(Field):
    """Descriptor of the FIX field OrderId."""

    def __set__(self, obj, value):
        """converts the value to str and sets its FIX value."""
        obj.__dict__[self.name] = fix.OrderID(str(value))


class OrderQty(Field):
    """Descriptor of the FIX field OrderQty."""

    def __set__(self, obj, value):
        """cconverts the value to float and sets its FIX value."""
        obj.__dict__[self.name] = fix.OrderQty(float(value))


class OrigClOrdID(Field):
    """Descriptor of the FIX field OrigClOrdID, the last accepted ClOrdID."""

    def __set__(self, obj, value):
        """converts the value to str and sets its FIX value."""
        obj.__dict__[self.name] = fix.OrigClOrdID(str(value))


class OrdType(Field):
    """Descriptor of the FIX field OrdType."""

    MARKET = fix.OrdType_MARKET
    LIMIT = fix.OrdType_LIMIT
    MARKET_WITH_LEFTOVER_AS_LIMIT = fix.OrdType_MARKET_WITH_LEFTOVER_AS_LIMIT
    STOP = fix.OrdType_STOP
    STOP_LIMIT = fix.OrdType_STOP_LIMIT
    STOP_LIMIT_MERVAL = "z"

    valid_values = [
        MARKET,
        LIMIT,
        MARKET_WITH_LEFTOVER_AS_LIMIT,
        STOP,
        STOP_LIMIT,
        STOP_LIMIT_MERVAL,
    ]

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        if value not in self.valid_values:
            raise ValueError(f"Invalid value, the valid values are {self.valid_values}")
        obj.__dict__[self.name] = fix.OrdType(value)


class Price(Field):
    """Descriptor of the FIX field Price."""

    def __set__(self, obj, value):
        """checks if value is positive and sets its FIX value."""
        if value < 0:
            raise ValueError("Price should be positive.")
        obj.__dict__[self.name] = fix.Price(value)


class SecurityExchange(Field):
    """Descriptor of the FIX field SecurityExchange."""

    ROFX = "ROFX"

    valid_values = [
        ROFX,
    ]

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        if value not in self.valid_values:
            raise ValueError(f"Invalid value, the valid values are {self.valid_values}")
        obj.__dict__[self.name] = fix.SecurityExchange(value)


class Side(Field):
    """Descriptor of the FIX field Side."""

    SELL = fix.Side_SELL
    BUY = fix.Side_BUY

    valid_values = [BUY, SELL]

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        if value not in self.valid_values:
            raise ValueError(f"Invalid value, the valid values are {self.valid_values}")
        obj.__dict__[self.name] = fix.Side(value)


class SubscriptionRequestType(Field):
    """Descriptor of the FIX field SubscriptionRequestType."""

    SNAPSHOT = fix.SubscriptionRequestType_SNAPSHOT
    SNAPSHOT_PLUS_UPDATES = fix.SubscriptionRequestType_SNAPSHOT_PLUS_UPDATES
    DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST = (
        fix.SubscriptionRequestType_DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST
    )

    valid_values = [
        SNAPSHOT,
        SNAPSHOT_PLUS_UPDATES,
        DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST,
    ]

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        if value not in self.valid_values:
            raise ValueError(f"Invalid value, the valid values are {self.valid_values}")
        obj.__dict__[self.name] = fix.SubscriptionRequestType(value)


class Symbol(Field):
    """Descriptor of the FIX field Symbol (or Instrument)."""

    def __set__(self, obj, value):
        """converts the value to string and sets its FIX value."""
        obj.__dict__[self.name] = fix.Symbol(str(value))


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================