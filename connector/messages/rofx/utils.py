#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from quickfix import OrdType_MARKET, OrdType_LIMIT, OrdType_STOP
from quickfix import OrdType_MARKET_WITH_LEFTOVER_AS_LIMIT, OrdType_STOP_LIMIT
from quickfix import SubscriptionRequestType_SNAPSHOT_PLUS_UPDATES


def type_is_snapshot_plus_update(subscription_req_type):
    """Utility to validate the  subscription request type."""
    return (
        subscription_req_type.getValue()
        == SubscriptionRequestType_SNAPSHOT_PLUS_UPDATES
    )


def get_order_type(ordtype: str):
    """A function to get the ordertype by name in str."""

    dict_order_types = {  # A  dict with the possible order types
        "market": OrdType_MARKET,
        "limit": OrdType_LIMIT,
        "market_with_leftover_as_limit": OrdType_MARKET_WITH_LEFTOVER_AS_LIMIT,
        "stop": OrdType_STOP,
        "stoplimit": OrdType_STOP_LIMIT,
        "stoplimitmerval": "z",
    }

    return dict_order_types[ordtype.lower()]


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================