#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from abc import ABC, abstractmethod
from datetime import datetime
import quickfix as fix


class Field(ABC):
    """
    An abstract descriptor class to use as Base for any Field.
    """

    @abstractmethod
    def __set__(self, obj, value):
        """
        Descriptor method to override.
        it modifies the object value when is setted.
        """
        pass

    def __get__(self, obj, type=None):
        """getter of the object."""
        return obj.__dict__.get(self.name)

    def __set_name__(self, owner, name):
        """sets the object variable name."""
        self.name = name


class Message(ABC):
    """
    An abstract class to be use as parent for any message.
    """

    @property
    @abstractmethod
    def required_fields(self):
        """Abstract property to store the required fields for the request."""
        pass

    @abstractmethod
    def is_valid(self):
        """Abstract method to verify if the request is valid."""
        pass

    @property
    @abstractmethod
    def message(self):
        """Abstract method to store the message."""  # probably it should be a property
        pass

    @staticmethod
    def new_message(msg_type, set_transact_time=False):
        """Creates a new FIX message."""

        message = fix.Message()
        message.getHeader().setField(fix.MsgType(msg_type))
        if set_transact_time:
            utcnow = (datetime.utcnow().strftime("%Y%m%d-%H:%M:%S.%f"))[:-3]
            message.getHeader().setField(fix.StringField(60, utcnow))
        return message


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================