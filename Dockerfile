# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

FROM python:3.8-slim-buster
# Python slim image, please refer to https://aka.ms/vscode-docker-python

ENV PYTHONDONTWRITEBYTECODE=1
# Keeps Python from generating .pyc files in the container

ENV PYTHONUNBUFFERED=1
# Turns off buffering for easier container logging

RUN apt-get update && apt-get install -y --no-install-recommends \
    g++ 
# Updates and installs the requirements for the container

WORKDIR /app
# Sets the work directory

COPY . /app
# Copy the entire project to the container

ADD requirements.txt /requirements.txt
# Copies the requirements

RUN python -m pip install quickfix
RUN python -m pip install -r requirements.txt
# Installs all the required python libraries, quickfix is apart because it fails if not

RUN useradd appuser && chown -R appuser /app
USER appuser
# Switching to a non-root user, please refer to https://aka.ms/vscode-docker-python-user-rights

# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================