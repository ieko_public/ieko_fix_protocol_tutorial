# IEKO FIX Protocol Tutorial - Shared Version of Fix Protocol Connector Tutorial

**Version 1.1 IEKO 2021**

This is an example of a FIX connector made in Python that uses the library [**Quickfix**](http://www.quickfixengine.org/) as FIX\
messaging engine and is *dockerized*.

**Our goal in sharing this code is to help understand how to handle the conection against**\
**a market using the FIX Protocol.**

_This project is licensed under the **GNU Lesser General Public License v3.0.**_ [Learn more](https://choosealicense.com/licenses/lgpl-3.0/)

**Hi! Welcome to IEKO**

We are software developers specialized in the financial market.\
We propose a new paradigm of **Sustainable Software.**

Through a scheme of GIT repositories, we constantly **release knowledge** and subproducts\
to be shared, reused and improved by the participants of the ecosystem.\
The contributions received as feedback are evaluated and, if they are considered valuable,\
incorporated.

The IEKO project is under development. **We invite you to participate**, there is a lot to\
discover together. 


_This project is shared under_ [***CopyLeft LGPL** license*](https://gitlab.com/ieko_public/ieko_proceso_publico/-/blob/master/LICENSE). When you use or share this project, **please**\
 **be responsible to the ecosystem** ;)

**IEKO Team**

-------- 

## Intro

**IEKO Public FIX Connector** is an CLI *(Command Line Interface)* Application that runs in\
a [*Docker*](https://docs.docker.com/) container



For this iteration of the connector we are connecting to a plataform called **[reMarkets](https://remarkets.primary.ventures/)**.\
*reMarkets* is a hyper-realistic market simulation platform and is ideal for learning how FIX\
and the markets work. You can learn more about this plataform in its *[website](https://remarkets.primary.ventures/)*

For now, it has connection to the ROFEX Market but, its prepared to support\
connectons to any market.

In the next iteration of **Young Talents** we will create an API version of this connector.

## What is FIX?


*Financial Information eXchange* (**FIX**) is a vendor-neutral electronic communications\
protocol for the international real-time exchange of securities transaction information.

FIX has become the de-facto messaging standard for pre-trade, trade, and post-trade\
communication.

## FIX Messages Format

The FIX Protocol uses a message encoding known as TagValue encoding, where each field\
consists of a unique numeric tag and a value.

Every FIX message is composed of a *header*, a *body* and a *trailer*.

The **header** consists of 5 mandatory fields and 1 optional, its followed by the **body** that\
its content depends on the MsgType(35) field declared in the header, and the last field\
(**trailer**) is the CheckSum(10).

Every Market has its own way to put the messages together.

### How do we know which fields and values the market wants?

Well, the markets provide us its **Rules of Engagement** (something like a user manual) that\
shows everything we need to interact with it.

By the other hand, quickfix gives us a [FIX11.xml](http://www.quickfixengine.org/FIXT11.xml) and a [FIX50SP2](http://www.quickfixengine.org/FIX50SP2.xml) files that we have to modify\
with the Market specs.
>*(these are stored in ./config/quickfix/xmls)*

We declare the files location (*transportdatadictionary*(FIX11) *appdatadictionary*(FIX50SP2)) in\
the .cfg file (`./config/rofx.dev.cfg`).

And now *Quickfix* that knows the market requirements, we can create our **FIX Messages** to **ROFX**\
I will show you how the MarketDataRequest message is build, but you can take a look to how\
every message is build starting in the file `./connector/messages/rofx/message_creator.py`

In first place we create all our fields with its possible values.
Example (`./connector/messages/rofx/fields.py`):

```python
class MDEntryType(Field):
    """Descriptor of the FIX field MDEntryType."""

    BID = fix.MDEntryType_BID
    OFFER = fix.MDEntryType_OFFER
    TRADE = fix.MDEntryType_TRADE
    OPENING_PRICE = fix.MDEntryType_OPENING_PRICE
    CLOSING_PRICE = fix.MDEntryType_CLOSING_PRICE
    SETTLEMENT_PRICE = fix.MDEntryType_SETTLEMENT_PRICE
    TRADING_HIGH_PRICE = fix.MDEntryType_TRADING_SESSION_HIGH_PRICE
    TRADING_LOW_PRICE = fix.MDEntryType_TRADING_SESSION_LOW_PRICE
    TRADE_VOLUME = fix.MDEntryType_TRADE_VOLUME
    OPEN_INTEREST = fix.MDEntryType_OPEN_INTEREST

    valid_values = [
        BID,
        OFFER,
        TRADE,
        OPENING_PRICE,
        CLOSING_PRICE,
        SETTLEMENT_PRICE,
        TRADING_HIGH_PRICE,
        TRADING_LOW_PRICE,
        TRADE_VOLUME,
        OPENING_PRICE,
    ]

    def __set__(self, obj, value):
        """checks if value is valid and sets its FIX value."""
        if value not in self.valid_values:
            raise ValueError(f"Invalid value, the valid values are {self.valid_values}")
        obj.__dict__[self.name] = fix.MDEntryType(value)
```

Then, the message format is created using the fields
Example (`./connector/messages/rofx/messages.py`):


```python
class MarketDataRequest(Message):
    """An model class for the market data request (MsgType V)."""

    ...

    @property
    def required_fields(self):
        """Returns the requires fields in the correct order as a list """
        fields = [
            self.instruments,
            self.MDReqID,
            self.SubscriptionRequestType,
            self.MarketDepth,
            self.AggregatedBook,
        ]

    ...

    @property
    def message(self):
        """Creates the Fix Message."""

        # Assert message is valid
        assert self.is_valid(), "Validate required fields."

        # ---Create-Message---
        message = Message.new_message(msg_type=fix.MsgType_MarketDataRequest)

        # ---Body---
        message.setField(self.MDReqID)
        message.setField(self.SubscriptionRequestType)
        message.setField(self.MarketDepth)
        if type_is_snapshot_plus_update(self.SubscriptionRequestType):
            message.setField(self.MDUpdateType)
        message.setField(self.AggregatedBook)

        # --- MDEntryTypes ---
        import quickfix50 as fix50

        group = fix50.MarketDataRequest().NoMDEntryTypes()

        group.setField(fix.MDEntryType(fix.MDEntryType_BID))
        message.addGroup(group)
        group.setField(fix.MDEntryType(fix.MDEntryType_OFFER))
        message.addGroup(group)
        group.setField(fix.MDEntryType(fix.MDEntryType_TRADE))
        message.addGroup(group)
        group.setField(fix.MDEntryType(fix.MDEntryType_OPENING_PRICE))
        message.addGroup(group)
        group.setField(fix.MDEntryType(fix.MDEntryType_CLOSING_PRICE))
        message.addGroup(group)

        # ---Instruments---
        symbol = fix50.MarketDataRequest().NoRelatedSym()
        for instrument in self.instruments:
            symbol.setField(fix.Symbol(instrument))
            message.addGroup(symbol)

        return message
```

And finally the message is fully created.
Example (`./connector/messages/rofx/message_creator.py`):

```python
def market_data_request(app: ROFXApplication, new_id: str, instrument: str):
    """
    Creates the market data request using the Messages and Fields descriptors.
    """

    order = messages.MarketDataRequest()
    order.SubscriptionRequestType = fix.SubscriptionRequestType_SNAPSHOT_PLUS_UPDATES
    order.MDReqID = str(new_id)
    order.MarketDepth = 5
    order.instruments = instrument
    order.MDUpdateType = fix.MDUpdateType_FULL_REFRESH
    order.AggregatedBook = True

    if order.is_valid():
        app.send(order.message)
        return True

    return False
```

#### You are invited to take a look to the full code and provide us with feedback!


## Initial Configurations

First things first... In order to use this connector, you will need to have the\
following things prepared:

* **Docker-compose** installed, if you dont have it, I'll leave here a \
[**link to install it**](https://docs.docker.com/engine/install/).

* [**reMarket**](https://remarkets.primary.ventures/) account, you can go to its [web](https://remarkets.primary.ventures/) and sign.

* Connector config file updated with your *reMarket account*.
> In the file `/config/ieko_public_fix_connector.yml` you need to complete the Username,\
>Password and Account values.
>```yaml
> # /config/ieko_public_fix_connector.yml
>...
>Recommendations:
>  Account: # Your reMarket Account
>  ...
>ROFX:
>  Fix:    
>  ...
>    Username: # Your reMarket User
>    Password: # Your reMarket Password
>...
>
>```

## Usage

So once we have everything ready, we can finally run the connector!!

Just go in your terminal to the root folder of the project and type:

``` bash
$ docker-compose run --rm app python main.py
```

>*note: if this doesnt work, try running with sudo or as root*

And thats it, if the IEKO Public FIX Connector is running you will see something\
like this:

```
---------------------------------------------------------
-----------Welcome to the IEKO's FIX Connector-----------
---------------------------------------------------------
To update the settings check the file '/config/ieko_public_fix_connector.yml' 


Please select an option: 
[1] Start!
[2] Quit!
:
```

## Useful links

Here im leaving all the links to the repos, tutorials, blogs, etc, that helped\
me in the development of this connector.

#### Medium and Github

* In first place the one that helps me to quickfix [**FIX Protocol in Python**](https://medium.com/@federico_dv/fix-protocol-en-python-primeros-pasos-e-implementaci%C3%B3n-29a130b71ffc).\
*A post in medium that is in spanish(I couldnt find anything like this in english).*

* Another really helpful post is [**FIX Protocol**](https://medium.com/@mdamelio/fix-protocol-bb7aaebeb2a7) by *@mdamelio*.\
*Also in spanish.*

* This [GitHub repo](https://github.com/rinleit/quickfix-python-samples) has a really good example of how to implements Quickfix.

* Also there is the official quickfix [documentation](http://www.quickfixengine.org/quickfix/doc/html/?quickfix/doc/html).

#### Youtube

* [playlist FIX API by @Darwinex](https://www.youtube.com/playlist?list=PLv-cA-4O3y95np0YK9NrKqNKLsORaSjBc)

---------

## We are [IEKO](http://ieko.io/)

We provide software solutions for *Fintech business* such as: Consulting, Custom\
Developments, Algorithmic Trading, Digital Onboarding, APIfication and integration\
of data sources.

Our goal is to build software being responsible with the technological ecosystem in\
which we exist, contributing to financial inclusion, making a concrete contribution to\
economic and social development.

#### Get in touch with us and know our projects.

* ieko.io
* [LinkedIn](https://www.linkedin.com/company/ieko-io)
* [Instagram](https://www.instagram.com/ieko.io/)
* [Twitter](https://twitter.com/ieko_io)
* [Facebook](https://www.facebook.com/ieko.io)
* contacto@ieko.io 

From Rosario, Argentina, to the world\
**#WeLoveToBuildSoftware**\
**#WeBuildSustainableSoftware**

--------
© Copyright 2021 IEKO - All rights reserved. | https://ieko.io/
