#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from connector.main import Connector
from signal import signal, SIGINT
import utils
import queue


def request_maker():
    """Command Line Interface that asks the values for the requests."""
    market: str = None
    request_type: str = None
    instrument: str = None
    clordid: str = None
    ordertype: str = None
    side: str = None
    account: str = None
    price: float = None
    origclorid: str = None
    orderqty: int = None

    while True:
        utils.clear()
        print("\nRemember that you can quit anytime by writing 'quit'.")
        utils.ieko_header()
        if not market:
            # utils.ieko_header()
            print("---------------------Select-A-Market---------------------")
            print(f"We have connectors for the following markets: {utils.valid_markets()}")
            market = input("\nEnter the FIX market you want to connect to: ")
            if market == "quit":
                request_type = "quit"
                break
            if not market.lower() in utils.valid_markets():
                market = None
            if not utils.config_is_valid(market):
                request_type = "invalid_config"
                utils.clear()
                break
            continue

        if not request_type:
            # utils.ieko_header()
            print("-----------------------Request-Type-----------------------")
            print("\nPlease choose one of the following requests:")
            print("[1] Get Market Data")
            print("[2] New Order Single")
            print("[3] Cancel Order")
            try:
                request_type = utils.valid_requests()[int(input(": "))]
                if request_type == "quit":
                    break
            except:
                request_type = None
            continue

        if not instrument:
            # utils.ieko_header()
            print("-------------------------Instruments-------------------------")
            print(f"Recommended instruments: {utils.recommended_instruments()}")
            print("-------------------------------------------------------------")
            print("Check the full list in: https://matbarofex.primary.ventures/\n")
            instrument = input("\nEnter an instrument(Symbol): ")
            if instrument == "quit":
                request_type = "quit"
                break
            continue

        if request_type in ["new_order", "cancel_order"]:
            if not clordid:
                # utils.ieko_header()
                print("----------------------Client-Order-ID----------------------")
                print("For convenience we use this format: ROFX-XXXXXXXX")
                clordid = input("\nCreates an client order ID for this request: ")
                if clordid == "quit":
                    request_type = "quit"
                    break
                continue

            if not ordertype:
                # utils.ieko_header()
                print("-------------------------Order-Type-------------------------")
                print("\nSelect one order type:")
                print("[1] market (currently rejected by ROFX)")
                print("[2] limit")
                print("[3] market_with_leftover_as_limit")
                print("[4] stop")
                print("[5] stop_limit")
                print("[6] stop_limit_merval")
                try:
                    ordertype = utils.valid_order_types()[int(input(": "))]
                    if ordertype == "quit":
                        request_type = "quit"
                        break
                except:
                    ordertype = None
                continue

            if not side:
                # utils.ieko_header()
                print("----------------------------Side----------------------------")
                side = input(f"\nEnter a side ({utils.valid_sides()}): ").upper()
                if side == "QUIT":
                    request_type = "quit"
                    break
                if not side in utils.valid_sides():
                    side = None
                continue

            if not account:
                # utils.ieko_header()
                print("--------------------------Account--------------------------")
                print(f"We recomend to use this account: {utils.recommended_account()}")
                account = input("\nEnter you FIX Account: ")
                if account == "quit":
                    request_type = "quit"
                    break
                continue

            if request_type == "new_order":
                if not price:
                    try:
                        # utils.ieko_header()
                        print("---------------------------Price---------------------------")
                        price = float(input("\nEnter a price: "))
                    except:
                        if price == "quit":
                            request_type = "quit"
                            break
                        print("\nPrice should be a number.")
                    continue

                if not orderqty:
                    try:
                        # utils.ieko_header()
                        print("-----------------------Order-Quantity-----------------------")
                        orderqty_input = input("\nEnter the quantity of the order: ")
                        orderqty = int(orderqty_input)
                    except:
                        if orderqty_input == "quit":
                            request_type = "quit"
                            break
                        print("\nOrder quantity should be an interger number.")
                    continue

            if request_type == "cancel_order":
                if not origclorid:
                    # utils.ieko_header()
                    print("---------------------Origin-Client-Order-ID---------------------")
                    origclorid = input(
                        "\nEnter the Client Order ID of the order you want to cancel: "
                    )
                    if origclorid == "quit":
                        request_type = "quit"
                        break
                    continue
        break

    context = {
        "market": market,
        "id": utils.random_string(),
        "type": request_type,
        "instrument": instrument,
        "clordid": clordid,
        "ordtype": ordertype,
        "side": side,
        "account": account,
        "price": price,
        "origclorid": origclorid,
        "orderqty": orderqty,
    }

    return context


if __name__ == "__main__":
    """This main will start the CLI and connector desired"""
    response_q = queue.Queue()
    signal(SIGINT, utils.handler)
    while True:
        print("---------------------------------------------------------")
        print("-----------Welcome to the IEKO's FIX Connector-----------")
        print("---------------------------------------------------------")
        print("visit us at www.ieko.com.ar------------------------------\n")
        print("Please keep updated the config file:")
        print("    ['/config/ieko_public_fix_connector.yml']\n\n")
        print("\nPlease select an option: ")
        print("[1] Start!")
        print("[2] Quit!")
        option = input(": ")
        response = ""
        connector = ""
        if option == "1":
            request = request_maker()
            if request['type'] == "invalid_config":
                print("#########################################################")
                print("########[Missing mandatory values in config file]########")
                print("#########################################################")
                continue
            elif request['type'] == "quit":
                utils.clear()
                continue
            connector = Connector(response_q, request["market"])
            request_q = connector.start()
            request_q.put(request)
            while not response:
                response = response_q.get()
                print("\n------------------------RESPONSE-------------------------\n")
                print(response["msg"])
                input("\nPress enter to go to main menu.\n")
                request_q.put({"type": "quit"})
        elif option == "2":
            if connector:
                request = {"type": "quit"}
                request_q.put(request)
            break
        if connector:
            connector.stop()
        utils.clear()

        
# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================