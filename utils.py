#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =======================================================================
# From: www.ieko.com.ar
# Author : kevin_ieko
# Project: Young_Talents
# =======================================================================
# This is a Example of a FIX connector made in Python that uses the
# library Quickfix as FIX messaging engine.
# Our goal in sharing this code is to help understand how to handle
# the conection against a market using the FIX Protocol.
# =======================================================================
# You are invited to meet IEKO, we have a lot to do together.
# Our entire project is collaborative and we are delighted to receive
# your comments, contributions, corrections, etc
# =======================================================================

from connector.settings import get_recommendations, config
from datetime import datetime
from os import system, name
import random
import string
import sys


months_dict = {
    "01": "Ene",
    "02": "Feb",
    "03": "Mar",
    "04": "Abr",
    "05": "May",
    "06": "Jun",
    "07": "Jul",
    "08": "Ago",
    "09": "Sep",
    "10": "Oct",
    "11": "Nov",
    "12": "Dic",
}


def recommended_account():
    """Returns a recommended account."""
    return get_recommendations().Account


def recommended_instruments():
    """Returns 3 recommended instruments."""
    month = datetime.now().strftime("%m")
    year = datetime.now().strftime("%y")
    instruments = []

    for i in range(3):
        if int(month) + i > 12:
            month = "00"
            year = str(int(year) + 1).zfill(2)
        month_name = months_dict[str(int(month) + i).zfill(2)]
        instruments.append(f"DO{month_name}{year}")

    return instruments


def valid_sides():
    """Returns a list with all the posible sides."""
    return get_recommendations().Sides


def valid_markets():
    """Returns a list with all the posible markets."""
    return get_recommendations().Markets


def valid_requests():
    """Returns a dict with all the posible requests."""
    return get_recommendations().Requests.toDict()


def valid_order_types():
    """Returns a dict with all the posible order types."""
    return get_recommendations().OrderTypes.toDict()


def config_is_valid(market):
    """Validates the mandatory config values."""
    if market.lower() == "rofx":
        print(config.ROFX.Fix.Username)
        if config.ROFX.Fix.Username and config.ROFX.Fix.Username:
            return True
    return False


def handler(signal_received, frame):
    """Handles the KeyboardInterrupt (Ctrl-C)"""
    clear()
    print("SIGINT or CTRL-C detected. Quiting...")
    sys.exit(0)


def clear():
    """Clears the terminal."""
    # for windows
    if name == "nt":
        _ = system("cls")
    # for mac and linux(here, os.name is 'posix')
    else:
        _ = system("clear")


def ieko_header():
    """Prints the ieko header in the console"""
    print("---------------------------------------------------------")
    print("----------------IEKO Public FIX Connector----------------")
    print("---------------------www.ieko.com.ar---------------------")
    print("---------------------------------------------------------\n")


def random_string(lenght=10):
    """Generates an random string."""
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(lenght))


# =======================================================================
# From Rosario, Argentina to the world
# #NosGustaHacerSoftware
# #HacemosSoftwareSustentable
# =======================================================================
# © Copyright 2020 IEKO - All rights reserved. | http://www.ieko.com.ar/
# =======================================================================